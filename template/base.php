<!--Created by Griffin Burkhardt-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="theme/theme.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="theme/pictures/icon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="theme/pictures/icon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123993378-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-123993378-1');
    </script>
</head>

<body>
<div id="content-container">
    <header>
        <a href="index.html"><div class="firstlast-title"><b>GRIFFIN</b> BURKHARDT</div></a>
        <div class="divider"></div>
        <nav id="top-nav">
            <ul id="top-nav-ul">
                <li><a <?php if ($page=="education.php") echo 'id="active-tab"'; ?> href="education.php">Education</a></li>
                <li><a <?php if ($page=="experience.php") echo 'id="active-tab"'; ?> href="experience.php">Experience</a></li>
                <li><a <?php if ($page=="projects.php") echo 'id="active-tab"'; ?> href="projects.php">Projects</a></li>
                <li><a href="resume.pdf" target="_blank">Résumé</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <?php echo $content; ?>
    </main>
    <footer>
        <div class="divider"></div>
        <nav id="bottom-nav">
            <ul id="bottom-nav-ul">
                <li><a href="https://www.linkedin.com/in/griffinburkhardt" target="_blank">LINKEDIN</a></li>
                <li><a href="https://bitbucket.org/griffinburkhardt" target="_blank">GITHUB</a></li>
                <li><a href="mailto:burkhardtg1@nku.edu">CONTACT</a></li>
                <li><a style="cursor: default">GRIFFIN &copy; 2018</a></li>
            </ul>
        </nav>
    </footer>
</div>
</body>
</html>