<!--Created by Griffin Burkhardt-->

<?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
<?php ob_start();?>

<head>
    <title>Griffin Burkhardt | Experience</title>
</head>

<div id="card-layout">

<!--ORGANIZATIONS-->
<div class="year-title">
    <div class="title">OCCUPATION</div>
</div>
<div class="divider-auto"></div>

<!--APPRENTICE-->
<div class="card card-large" id="cai-card"><a href="https://inside.nku.edu/informatics/centers/cai.html" target="_blank">
    <div class="card-content">
        <div class="project-title-white">APPRENTICE</div>
        <div class="project-title-org-white">Center for Applied Informatics</div>
        <div class="project-desc-white">high-impact acedemic and virtual co-op which engages students in informatics initiatives for organizations locally, regionally, nationally, and internally by tasking them to build and/or improve solutions in the technology spectrum by utilizing&nbsp;
            <span class="p-tools">mobile, web, and creative development</span>
        </div>
    </div>
</a></div>

<!--INFO-->
<div class="card card-xsmall-horz" id="cai-blur-card">
    <div class="card-content" style="padding: 10px 20px;">
        <div class="project-desc-white" style="margin: 0px;">
            <span class="p-tools">
            <ul class="info-card">
                <li><b>skillset</b> ui/ux development</li>
                <li class="info-card-spacer">|</li>
                <li><b>track</b> project management</li>
                <li class="info-card-spacer">|</li>
                <a href="mailto: samesm1@nku.edu" style="color: #F5CDC1;"><li><b>manager</b> mike sames</li></a>
            </span>
        </div>
    </div>
</div>

<!--HACKATHONS-->
<div class="year-title">
    <div class="title">HACKATHONS</div>
</div>
<div class="divider-auto"></div>

<!--CATHACKS-->
<div class="card card-small-horz" id="tweet-card"><a href="https://www.linkedin.com/in/griffinburkhardt/detail/treasury/position:1283372027/?entityUrn=urn%3Ali%3Afs_treasuryMedia%3A(ACoAAB8RHYsB0VbBTAA289mA3mtfAbzYn9YXrR4%2C1524059526447)&section=position%3A1283372027&treasuryCount=3" target="_blank">
    <div class="card-content">
        <div class="project-title-black">CATHACKS IV</div>
        <div class="project-title-org">UI/UX Developer</div>
        <div class="project-desc-gray">developed the user interface/user experience using modern web techniques, such as, flexbox and minimalistic design, along with testing quality assurance/use-case scenarios for&nbsp;
            <span class="p-tools">#tweet_waves</span>
        </div>
        <div class="project-sub-title">— spring 2018 —</div>
    </div>
</a></div>

</div>

<?php
$content = ob_get_clean();
include "template/base.php";?>