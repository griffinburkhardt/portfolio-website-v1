<!--Created by Griffin Burkhardt-->

<?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
<?php ob_start();?>

<head>
    <title>Griffin Burkhardt | Projects</title>
</head>

<div id="card-layout">

<!--2018 — 2019-->
<div class="year-title">
    <div class="year">2018 — 2019</div>
    <div class="grade">JUNIOR</div>
</div>
<div class="divider-auto"></div>

<!--#TWEET_WAVES-->
<div class="card card-small-horz" id="norsetime-card">
    <div class="card-content">
        <div class="project-title-black">NORSETIME</div>
        <div class="project-title-org">UI/UX Developer</div>
        <div class="project-sub-title">— currently in development —</div>
        <div class="project-desc-gray">my team at the Center for Applied Informatics developed a customizable employee skills section into our NorseTime web application for users to search and select their skills, assign a rating, and be sorted by a manager skill search tool using&nbsp;
            <span class="p-tools">html/css, php, mysql, and javascript</span>
        </div>
    </div>
</div>

<!--#TWEET_WAVES-->
<div class="card card-small-horz" id="tweet-card">
    <div class="card-content">
        <div class="project-title-black">#TWEET_WAVES</div>
        <div class="project-title-org">UI/UX Developer</div>
        <div class="project-sub-title">— currently in development —</div>
        <div class="project-desc-gray">my team during the University of Kentucky CatHacks IV hackathon created an application that collects thousands of content based tweets to form a heatmap of areas of popularity using&nbsp;
            <span class="p-tools">html/css, php, mysql, python, and api</span>
        </div>
    </div>
</div>
    
<!--2017 — 2018-->
<div class="year-title">
    <div class="year">2017 — 2018</div>
    <div class="grade">SOPHOMORE</div>
</div>
<div class="divider-auto"></div>

<!--PORTFOLIO WEBSITE-->
<div class="card card-small" id="portfolio-card">
    <div class="card-content">
        <div class="project-title-black">PORTFOLIO WEBSITE</div>
        <div class="project-desc-gray">built a portfolio to practice modern UI and domain maintenance using&nbsp;
            <span class="p-tools">html/css, php, and cpanel</span>
        </div>
    </div>
</div>

<!--MUSIC COMPOSITION-->
<div class="card card-small" id="fl-card"><a href="https://soundcloud.com/griffinproduction" target="_blank">
    <div class="card-content">
        <div class="project-title-white">MUSIC COMPOSITION</div>
        <div class="project-desc-white">compose my own music ranging from electronic to orchestral using&nbsp;
            <span class="p-tools">fl studio 12 producer edition</span>
        </div>
    </div>
</a></div>

<!--TESLA WEBSITE-->
<div class="card card-large" id="tesla-card"><a href="http://studenthome.nku.edu/~burkhardtg1/tesla/home.html" target="_blank">
    <div class="card-content">
        <div class="project-title-black">TESLA WEBSITE</div>
        <div class="project-desc-gray">created my own mirrored version of the Tesla website from scratch to practice modern UI layouts, responsive UX, and minimalist design while creating a consumer friendly browsing experience using&nbsp;
            <span class="p-tools">html/css and javascript</span>
        </div>
    </div>
</a></div>

<!--2016 — 2017-->
<div class="year-title">
    <div class="year">2016 — 2017</div>
    <div class="grade">FRESHMAN</div>
</div>
<div class="divider-auto"></div>

<!--GITHUB-->
<div class="card card-xsmall-horz" id="github-card"><a href="https://bitbucket.org/burkhardtg1" target="_blank">
    <div class="card-content">
        <div class="project-title-black">GITHUB</div>
        <div class="project-desc-gray">created a GitHub account which hosts all of my projects using&nbsp;
            <span class="p-tools">atlassian bitbucket</span>
        </div>
    </div>
</a></div>
</div>

<?php
$content = ob_get_clean();
include "template/base.php";?>