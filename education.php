<!--Created by Griffin Burkhardt-->

<?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
<?php ob_start();?>

<head>
    <title>Griffin Burkhardt | Education</title>
</head>

<div id="card-layout">

<!--ORGANIZATIONS-->
<div class="year-title">
    <div class="title">UNIVERSITY</div>
</div>
<div class="divider-auto"></div>

<!--BUSINESS INFORMATICS-->
<div class="card card-large" id="bi-card"><a href="https://www.nku.edu/academics/informatics/programs/undergraduate/bis.html" target="_blank">
    <div class="card-content">
        <div class="project-title-white">BUSINESS INFORMATICS, B.S.</div>
        <div class="project-title-org-white">Northern Kentucky University</div>
        <div class="project-desc-white">businesses across the globe depend heavily on advanced information systems for managing information and business operations competitively — this calls for information systems professionals to manage and improve an organization’s knowledge and capabilities by combining&nbsp;
            <span class="p-tools">business, management, and information technology</span>
        </div>
    </div>
</a></div>

<!--INFO-->
<div class="card card-xsmall-horz" id="blur-card">
    <div class="card-content" style="padding: 10px 20px;">
        <div class="project-desc-white" style="margin: 0px;">
            <span class="p-tools">
            <ul class="info-card">
                <li><b>gpa</b> 3.67</li>
                <li class="info-card-spacer">|</li>
                <li><b>minor</b> business</li>
                <li class="info-card-spacer">|</li>
                <li><b>track</b> cyber security</li>
                <li class="info-card-spacer">|</li>
                <li><b>graduating</b> may 2020</li>
                <li class="info-card-spacer">|</li>
                <li><b>year</b> junior</li>
            </span>
        </div>
    </div>
</div>

<!--ORGANIZATIONS-->
<div class="year-title">
    <div class="title">ORGANIZATIONS</div>
</div>
<div class="divider-auto"></div>

<!--COI AMBASSADOR-->
<div class="card card-small"><a href="https://inside.nku.edu/informatics/beyond/coiambassadors.html" target="_blank">
    <div class="card-content">
        <div class="project-title-black">COI AMBASSADORS</div>
        <div class="project-title-org">College of Informatics</div>
        <div class="project-desc-gray">academic organization by invitation only which lends student assistance to college events, advisory boards, regional tech-based events, and more
        </div>
        <div class="project-sub-title">— 2017 to present —</div>
    </div>
</a></div>

<!--STUDENT MENTEE-->
<div class="card card-small">
    <div class="card-content">
        <div class="project-title-black">STUDENT MENTEE</div>
        <div class="project-title-org">Procter &amp; Gamble</div>
        <div class="project-desc-gray">connects prospective students with P&amp;G NKU alumni to develop interpersonal communication skills, networking abilities, and professionalism
        </div>
        <div class="project-sub-title">— fall 2017 —</div>
    </div>
</div>

<!--ACM-->
<div class="card card-small-horz">
    <div class="card-content">
        <div class="project-title-black">ASSN. FOR COMPUTING MACHINERY</div>
        <div class="project-title-org">Northern Kentucky University</div>
        <div class="project-desc-gray">delivers resources that advance computing as a science and a profession, enable professional development, and promote policies and research that benefit society
        </div>
        <div class="project-sub-title">— 2018 to present —</div>
    </div>
</div>

<!--VOLUNTEERING-->
<div class="year-title">
    <div class="title">VOLUNTEERING</div>
</div>
<div class="divider-auto"></div>

<!--DATA ENTRY-->
<div class="card card-small">
    <div class="card-content">
        <div class="project-title-black">DATA ENTRY</div>
        <div class="project-title-org">FLL Robotics Championship</div>
        <div class="project-desc-gray">received numerical scores handed over by the judges and inputted them into an Excel spreadsheet while organizing and sorting each group via their average score
        </div>
    </div>
</div>

<!--JUDGE-->
<div class="card card-small">
    <div class="card-content">
        <div class="project-title-black">PROJECT JUDGE</div>
        <div class="project-title-org">Academy Scholars of Informatics</div>
        <div class="project-desc-gray">graded students on professionalism, whether they accomplished the set goals, how they utilized the game engine, and the overall completeness of their video game
        </div>
    </div>
</div>

<!--CERTIFICATES-->
<div class="year-title">
    <div class="title">CERTIFICATES</div>
</div>
<div class="divider-auto"></div>

<!--MACHINE LEARNING-->
<div class="card card-xsmall-horz" id="ml-card"><a href="https://www.coursera.org/account/accomplishments/verify/HN3H5HSYYKJD" target="_blank">
    <div class="card-content">
        <div class="project-title-black">MACHINE LEARNING</div>
        <div class="project-desc-gray">covers how to apply the most advanced machine learning algorithms to real world internet of things problems taught by&nbsp;
            <span class="p-tools">coursera at stanford university</span>
        </div>
    </div>
</a></div>

<!--MACHINE LEARNING-->
<div class="year-title">
    <div class="title">AWARDS</div>
</div>
<div class="divider-auto"></div>

<!--CATHACKS-->
<div class="card card-xsmall-horz" id="tweet-card"><a href="https://www.linkedin.com/in/griffinburkhardt/detail/treasury/position:1283372027/?entityUrn=urn%3Ali%3Afs_treasuryMedia%3A(ACoAAB8RHYsB0VbBTAA289mA3mtfAbzYn9YXrR4%2C1524059526680)&section=position%3A1283372027&treasuryCount=3" target="_blank">
    <div class="card-content">
        <div class="project-title-black">CATHACKS IV SPONSOR WINNER</div>
        <div class="project-desc-gray">awarded the "big data hack" sponsored prize presented by HP's OpenText service for the utilization of social data analytics at&nbsp;
            <span class="p-tools">cathacks iv hackathon</span>
        </div>
    </div>
</a></div>

<!--DEAN'S LIST-->
<div class="card card-xxsmall">
    <div class="card-content">
        <div class="project-title-black">DEAN'S LIST</div>
        <div class="project-sub-title">— spring 2017 —</div>
    </div>
</div>

<!--PRESIDENT'S HONORS LIST-->
<div class="card card-xxsmall-fill">
    <div class="card-content">
        <div class="project-title-black">PRESIDENT'S HONORS LIST</div>
        <div class="project-sub-title">— fall 2017 —</div>
    </div>
</div>
</div>

<?php
$content = ob_get_clean();
include "template/base.php";?>